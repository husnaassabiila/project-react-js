import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ContactP from "./components_landingpage/contact-page";

const App = () => {

  return (
    <>

    <BrowserRouter>
      <Routes>
        
        <Route path="/hubungi-kami" element={ <ContactP /> } />

      </Routes>
    </BrowserRouter>
    
    </>
  );
}

export default App;
