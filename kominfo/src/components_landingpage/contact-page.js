import React from "react";
import NavbarP from "./navbar";
import FooterP from "./footer";
import HeroSectionP from "./herosection";

const ContactP = () => {
    return(
        <>
        
        <NavbarP />
        <HeroSectionP />
        <FooterP />
        
        </>
    );
};

export default ContactP;